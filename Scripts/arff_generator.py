#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  arff_generator.py
#  
#   Generate arff format attributes for import functions from a csv file.
#   The assumprion is that the csv file will have a header where all the 
#   columns except last one are continues values and final one is 
#   nominal class attribute
#  

import argparse
import os.path
from sys import argv

def print_header(fout, dataset_desc):
    fout.write('%1. Title: Microsoft Malware Classification Challenge DataSet Instruction Frequency\n')
    fout.write('%\n')
    fout.write('@RELATION some_%s\n' % dataset_desc)
    
def print_arff_attributes(csv_header, fout, no_class, has_id_column, mask = None):
    cols = csv_header.strip().split(',')
    
    for i in range(0, len(cols)):
        if mask is not None and i not in mask:
            continue
            
        if i==0 and has_id_column:
            fout.write('@ATTRIBUTE ' + cols[i] + ' STRING\n')
            continue
            
        if not no_class and i == len(cols) - 1:
            fout.write('@ATTRIBUTE class  {1,2,3,4,5,6,7,8,9}\n')
        else:
            s = '@ATTRIBUTE ' + cols[i] + ' NUMERIC\n'
            fout.write(s)
    
    if no_class:
        fout.write('@ATTRIBUTE class  {1,2,3,4,5,6,7,8,9}\n')
        
    fout.write('\n')

def print_arff_attributes_dll_boolean(csv_header, fout, no_class, has_id_column, mask = None):
    cols = csv_header.strip().split(',')
    
    for i in range(0, len(cols)):
        if mask is not None and i not in mask:
            continue
            
        if i==0 and has_id_column:
            fout.write('@ATTRIBUTE ' + cols[i] + ' STRING\n')
            continue
            
        if not no_class and i == len(cols) - 1:
            fout.write('@ATTRIBUTE class  {1,2,3,4,5,6,7,8,9}\n')
        else:
            s = '@ATTRIBUTE ' + cols[i] + ' {0,1}\n'
            fout.write(s)
    
    if no_class:
        fout.write('@ATTRIBUTE class  {1,2,3,4,5,6,7,8,9}\n')
        
    fout.write('\n')
    
def print_body(fout, line, mask=None, no_class=False):
    if mask is None and not no_class:
        # If all atributes are needed and class label is in original line,
        # then print line as is.
        fout.write(line)
    elif mask is None and no_class:
        line = line.strip() + ',?\n' 
        fout.write(line)
    elif mask is not None:
        cols = line.strip().split(',')
        line = ''
        first = True
        for i in range(0, len(cols)):
            if i not in mask:
                continue
            if not first:
                line = line + ','
            
            first = False
            line = line + cols[i]
        
        if no_class:
            line = line.strip() + ',?' 
        
        fout.write(line + '\n')
    
def print_body_dll_func_freq(fout, input_path):
    # First Pass: identify all the non-zero frequecy values
    # mask[i] will be True if atleast one value in that column is non-zero
    mask = {}
    with open(input_path) as fin:
        line_count = 0
        for line in fin:
            line_count = line_count + 1
            if line_count == 1:
                continue
            
            cols = line.strip().split(',')
            if len(cols) == 0:
                continue
                
            for i in range(0, len(cols) - 1):
                if cols[i] != '0.0':
                    mask[i] = True
            
            mask[len(cols) - 1] = True
    
    # Second Pass: write the frequecy values of columns in mask, which includes class label column
    with open(input_path) as fin:
        line_count = 0
        for line in fin:
            line_count = line_count + 1
            if line_count == 1:
                print_arff_attributes(line, fout, mask)
                continue
            
            if line_count == 2:
                fout.write('@DATA\n')
            
            cols = line.strip().split(',')
            for i in range(0, len(cols)):
                if i not in mask:
                    continue
                
                fout.write(cols[i])
                if i != len(cols)-1:
                    fout.write(',')
            
            fout.write('\n')

def print_body_dll_boolean(fout, input_path, no_class=False, has_id_column=False, skip_id = False):
    """
    Generate ARFF file for dll boolean feature. 
    """
    mask = None
    with open(input_path) as fin:
        line_count = 0
        for line in fin:
            line_count = line_count + 1
            if line_count == 1:
                if skip_id:
                    # if table hash ID column and we want to skip it,
                    # then set mask bit for all columns except id_column
                    mask = {}
                    cols = line.strip().split(',')
                    for i in range(1, len(cols)):
                        mask[i] = 1
                    
                print_arff_attributes_dll_boolean(line, fout, no_class, has_id_column, mask=mask)
                continue
            
            if line_count == 2:
                fout.write('@DATA\n')
            
            print_body(fout, line, mask=mask, no_class=no_class)


def print_body_ngram_freq(fout, input_path, no_class=False, has_id_column=False, skip_id = False):
    mask = None
    with open(input_path) as fin:
        line_count = 0
        for line in fin:
            line_count = line_count + 1
            if line_count == 1:
                if skip_id:
                    # if table hash ID column and we want to skip it,
                    # then set mask bit for all columns except id_column
                    mask = {}
                    cols = line.strip().split(',')
                    for i in range(1, len(cols)):
                        mask[i] = 1
                    
                print_arff_attributes(line, fout, no_class, has_id_column, mask=mask)
                continue
            
            if line_count == 2:
                fout.write('@DATA\n')
            
            print_body(fout, line, mask=mask, no_class=no_class)

def main():
    parser = argparse.ArgumentParser(description='converts a .csv file to .arff file.')
    parser.add_argument('-in', required=True, dest='input_path', help='path to input csv file.')
    parser.add_argument('-out', required=True, dest='output_path', help='path to output arff file.')
    parser.add_argument('-no-class', type=bool, default=False, dest='no_class', help='True if data instances do not contain any class label column.')
    parser.add_argument('-id-column', type=bool, default=False, dest='id_column', help='True if first column in data tabel is the instance id column.')
    parser.add_argument('-skip-id', type=bool, default=False, dest='skip_id', help='True if we don NOT want to include id_column in .arff file, given that first column in data tabel is the instance id column.')
    parser.add_argument('-desc', default='', dest='dataset_desc', help='Description of the data set.')
    parser.add_argument('-mode', type=str, default='ngram', dest='run_mode', help='running mode {ngram | dll}')
    
    # parse command line args 
    args = parser.parse_args()
    
    if os.path.exists(args.output_path):
        print "Output file '%s' already exist. Delete first then run." % args.output_path
        return 1
        
    fout = open(args.output_path, 'w')
    
    print_header(fout, args.dataset_desc)
    
    if args.run_mode == 'ngram':
        print_body_ngram_freq(fout, args.input_path, no_class=args.no_class, has_id_column=args.id_column, skip_id=args.skip_id)
    elif args.run_mode == 'dll':
        print_body_dll_boolean(fout, args.input_path, no_class=args.no_class, has_id_column=args.id_column, skip_id=args.skip_id)
    else:
        print 'Specify RUN_MODE'
            
    return 0

if __name__ == '__main__':
	main()

