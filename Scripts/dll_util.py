#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  dll_util.py
#  
#  Copyright 2015 meha <meha@meha-VPCSB16FG>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import re

class ImportTableInfo:
    def __init__(self, function_name = None, library_name = ''):
        self.function_name = function_name
        self.library_name = library_name


    @staticmethod
    def parse_line(line):
        import_library_regex = r'idata:[0-9A-Fa-f]+\s+;\s+Imports from\s+(\S+)'
        import_function_regex = r'idata:[0-9A-Fa-f]+\s+;\s+[A-Za-z]+\s+__[A-Za-z]+\s+([A-Za-z]+)\('
        
        regex_result = re.findall(import_function_regex, line)
        is_function = True
        
        if len(regex_result) < 1 or len(regex_result[0]) < 1:
            is_function = False
            regex_result = re.findall(import_library_regex, line)
            if len(regex_result) < 1 or len(regex_result[0]) < 1:
                return None
        
        if is_function:
            return ImportTableInfo(function_name = regex_result[0].strip())
        else:
            return ImportTableInfo(library_name = regex_result[0].strip())
