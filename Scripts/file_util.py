#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  file_util.py
#  
#  Copyright 2014 Mehadi
#  
#  This program exposes helper classes for csv text file reading
#  and writing.  
#  
#  

from sys import stderr
from sys import argv
from sets import Set
import os.path
from os import listdir
from os.path import isfile, isdir, join
import operator

############################# data util ################################
class DataReader:
    """
    Reads data from all files in a directory or from a single file.
    """
    
    def __init__(self, data_path, verbous=False, first_row_header = False):
        """
        Constructor
        
        PARAMETERS:
            data_path - can either be a single file. Or path to the 
                folder containing files with data. Note: does not 
                go recursively into subfolders if any.
        """
        self.filepath_list = None
        if isfile(data_path):
            self.filepath_list = [data_path]
        else:
            self.filepath_list = [join(data_path, f) for f in listdir(data_path) if isfile(join(data_path, f))]
        # sort files by name, the names 
        self.filepath_list.sort();
            
        # current file object
        self.current_file = None
        self.verbous = verbous
        self.first_row_header = first_row_header
        
    def data_line_iterator(self, data_parser = None, file_changed_callback_obj = None, file_extension = None):
        """
        Iterator over all the lines in all data files.
        Parameters:
            data_parser - if data parser method is provided the iterator 
                 will yield the return of data_parser
            file_changed_callback_obj - an object with file_changed_callback
                function. this will be called when ever the file being 
                read changes.
            file_extension - Only files with this file_extension will be read.
        """
        for fpath in self.filepath_list:
            if file_extension is not None and not os.path.basename(fpath).endswith(file_extension):
                continue
            if not os.path.exists(fpath):
                continue
            with open(fpath) as f:
                if self.verbous:
                    print 'Reading file %s' % fpath
                prev_filepath = self.current_file
                self.current_file = fpath
                self.current_line = -1
                if file_changed_callback_obj != None:
                    file_changed_callback_obj.file_changed_callback(prev_filepath, self.current_file)
                for line in f:
                    self.current_line = self.current_line + 1
                    if self.first_row_header and self.current_line == 0:
                        continue
                    if data_parser == None:
                        yield line.strip()
                    else:    
                        yield data_parser(line.strip())
                
                    
    def get_current_position(self):
        return (self.current_file, self.current_line)
      
class DataWriter:
    """
    Helper class for taking care of writing to files.
    You can specify the maximum number of records(lines) you want written
    per file, and DataWriter will aoutomatically create the next file
    (with the filename given as output_path_pattern and file index appended
    to it) and continue writing to the new file.  
    """
    def __init__(self, output_path_pattern, print_header = True, file_extension = '.csv', max_file_lines = 400000):
        fullpath = os.path.realpath(output_path_pattern)
        if isdir(fullpath):
            self.file_pattern = 'output_'
            self.output_dir = fullpath
        else:    
            self.file_pattern = os.path.basename(fullpath)
            self.output_dir = os.path.dirname(fullpath) 
            
        self.file_count = 0
        self.line_count = 0
        self.max_file_lines = max_file_lines
        self.current_file = None
        self.print_header = print_header
        self.file_extension = file_extension
        
    def open_next(self):
        if self.current_file != None:
            self.current_file.close()
        
        filename = os.path.join(self.output_dir, 
                        self.file_pattern + '%04d' % self.file_count + self.file_extension)
        self.current_file = open(filename, 'w')
        self.file_count = self.file_count + 1
        self.line_count = 0
        
    def write(self, obj):
        if self.current_file is None:
            self.open_next()
        if self.max_file_lines > 0 and self.line_count >= self.max_file_lines:
            self.open_next()
        if self.line_count == 0 and self.print_header:
            self.current_file.write(obj.header() + '\n')
        
        self.current_file.write(str(obj) + '\n')
        self.line_count = self.line_count + 1
    
    def write_str_line(self, print_str):
        self.write_str(print_str)
        self.current_file.write('\n')
        
    def write_str(self, print_str):
        if self.current_file is None:
            self.open_next()
        if self.max_file_lines > 0 and self.line_count >= self.max_file_lines:
            self.open_next()
        
        self.current_file.write(print_str)
        self.line_count = self.line_count + 1
        
        
    def close(self):
        if self.current_file != None:
            self.current_file.close()
        

def main():
	
	return 0

if __name__ == '__main__':
	main()

