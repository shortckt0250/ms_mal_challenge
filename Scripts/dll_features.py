#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  dll_features.py
#  
#  Copyright 2015 meha <meha@meha-VPCSB16FG>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import argparse
import os.path
import re

from dataset_util import DataSetUtil
from dll_util import ImportTableInfo
from file_util import DataReader
from file_util import DataWriter


########################## Import table Reader #########################
class DllImportFeatureProcessor:
    imported_func_list_file = "imported_func_list"
    def __init__(self, training_label_file, dll_list_file, print_row_id=False):
        # mapping of instance_id -> class lable
        self.training_label = None
        if training_label_file is not None:
            self.training_label = DataSetUtil.read_training_lable(training_label_file)
        self.dll_list = self.read_dll_list_file(dll_list_file)
        # If True, The first column of each row is the rowId.
        self.print_row_id = print_row_id


    def resolve_class_label(self, filename):
        """
        Returns the class label of the binary file from filename.
        Returns None if class label can not be resolved from filename.  
        """
        if filename in self.training_label and self.writer is not None:
            return self.training_label[filename]
            
        return None


    def read_dll_list_file(self, dll_list_file):
        dll_list = []
        with open(dll_list_file) as fin:
            for line in fin:
                line = line.strip()
                if len(line) == 0:
                    continue
                dll_list.append(line)
        
        return dll_list

    def reset_func_freq(self):
        """
        Resets all the counts to zero.
        """
        self.imported_dlls = {}


    def file_changed_callback(self, prev_filepath, curr_filepath):
        """
        DataReader reading file changed event handler(callback function).
        """
        self.write_func_freq_vect_to_file(prev_filepath)
        
        self.filecount = self.filecount + 1
        if self.filecount % 200 == 0:
            print "%d Files processed..." % (self.filecount)


    def write_func_freq_vect_to_file(self, prev_filepath):
        if prev_filepath is None:
            return  # First file just opened.
        in_filename = os.path.basename(prev_filepath)
        # get the hash representing the malware id from file name
        exe_hash = in_filename.split()[0].split('.')[0]
        if self.training_label is not None:    
            class_label = self.resolve_class_label(exe_hash)
            if class_label == None:
                return
        else:
            class_label = None # in case of test data without class label.
        
        feat_vect = self.get_boolean_feature_vector(class_label, exe_hash)
        feat_header = self.get_boolean_feature_vector_header(has_class=(class_label is not None))
        feat_vect_obj = DllBooleanFeature(feat_header, feat_vect)
        self.writer.write(feat_vect_obj)
        self.reset_func_freq()


    def get_boolean_feature_vector(self, class_label, row_id):
        """
        Returns a boolean feature vector, specifying wheather a dll 
        is imported by binary or not.
        Parameters:
            class_label - the class label of data instance. If this is 
                          set to None, then class label will not be added.
        """
        if self.print_row_id:
            f_vector = row_id
            first = False
        else:
            f_vector = ''
            first = True
        
        for dll in self.dll_list:
            if not first:
                f_vector = f_vector + ','
            
            if dll in self.imported_dlls:
                val = '1'
            else:
                val = '0'
            
            f_vector = f_vector + val
            first = False
        
        if class_label is not None:
            f_vector = f_vector + ',' + class_label
        return f_vector


    def get_boolean_feature_vector_header(self, has_class=True):
        """
        Returns the header of instruction freq table.
        """
        if self.f_vector_header is not None:
            return self.f_vector_header
            
        if self.print_row_id:
            self.f_vector_header = 'row_id'
            first = False
        else:    
            self.f_vector_header = ''
            first = True
        
        for dll in self.dll_list:
            if not first:
                self.f_vector_header = self.f_vector_header + ','
            
            self.f_vector_header =  self.f_vector_header + dll  
            first = False
        
        if has_class:
            self.f_vector_header = self.f_vector_header + ',class'
        return self.f_vector_header


    def extract_dll_boolean_feature(self, input_path, output_path, max_files=-1):
        """
        Extractes dll import boolean feature.
        """
        self.reader = DataReader(input_path)
        self.writer = DataWriter(output_path)
        self.imported_dlls = {}
        # Numebr of files proccessed
        self.filecount = 0
        # CSV header row for table created using these features
        self.f_vector_header = None
        
        for import_info in self.reader.data_line_iterator(data_parser = ImportTableInfo.parse_line, file_changed_callback_obj = self, file_extension = '.asm'):
            if import_info == None:
                continue
        
            if max_files != -1 and self.filecount >= max_files:
                break
            
            if import_info.library_name is not '':
                self.imported_dlls[import_info.library_name.split('.')[0]] = 1

        self.writer.close()
        
########################################################################
class DllBooleanFeature:
    def __init__(self, feat_header, feat_vect):
        self.feat_header = feat_header
        self.feat_vect = feat_vect
        
    
    def header(self):
        return self.feat_header
        
        
    def __str__(self):
        return self.feat_vect
        
 
######################################################################## 
def main():
    # command line args
    parser = argparse.ArgumentParser(description='Preprocess disassembled  malware file to generate boolean dll features.')
    parser.add_argument('-in', required=True, dest='input_path', help='path to disassembled malware file or directory.')
    parser.add_argument('-label', type=str, default=None, dest='training_label_file', help='path to file containing the training label file')
    parser.add_argument('-dll-list', type=str, default=None, dest='dll_list_file', help='path to file containing list of all imported dlls in training set, one per line.')
    parser.add_argument('-out', required=True, dest='output_path', help='path to output file or directory.')
    parser.add_argument('-rowid', type=bool, default=False, dest='include_row_id', help='Boolean value to include row_id column.')
    parser.add_argument('-max', type=int, default=-1, dest='max_files', help='max number of assembly files to process.')
    
    # parse command line args
    args = parser.parse_args()
    
    processor = DllImportFeatureProcessor(args.training_label_file, args.dll_list_file, args.include_row_id)
    processor.extract_dll_boolean_feature(args.input_path, args.output_path, max_files=args.max_files)
    
    return 0

if __name__ == '__main__':
    main()

