#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  instruction_util.py
#  
#  Copyright 2015 meha <meha@meha-VPCSB16FG>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
#  

import re


class OpcodeInstPair:
    def __init__(self, opcode, inst):
        self.opcode = opcode
        self.inst = inst
        
    def header(self):
        return 'opcode,inst'
        
    def get_opcode(self):
        """
        Returns the instruction opcode. One byte or two byte version.
        """
        if len(self.opcode) < 2:
            return None
        
        # determin if the instruction has 1-byte or 2-byte opcode
        # Note 2-byte opcodes start with 'OF' prefix 
        result = re.findall(r'^0[Ff]\s+[0-9A-Fa-f]{2}', self.opcode)
        if len(result) == 0:
            return self.opcode[:2]
        else:            
            return result[0]
        
    @staticmethod
    def parser_line(line):
        regex_opcode_inst = r'.text:[0-9A-F]+\s+((?:[0-9A-F][0-9A-F]\s)+)\s+(\S+)'
        
        regex_result = re.findall(regex_opcode_inst, line)
        
        if len(regex_result) < 1 or len(regex_result[0]) < 2:
            return None
        
        opcode_i = 0
        inst_i = 1
        return OpcodeInstPair(regex_result[0][opcode_i].strip(), regex_result[0][inst_i])
        
    def __str__(self):
        return self.opcode + ',' + self.inst



def main():
	
	return 0

if __name__ == '__main__':
	main()

