#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  opcode_inst_extractor.py
#  
#  Copyright 2015 mehadi
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import os.path
from os import listdir
import re
from sys import argv

from file_util import DataReader
from file_util import DataWriter
from archive_util import ArchiveUtil
from dataset_util import DataSetUtil
from instruction_util import OpcodeInstPair


########################## Import table Reader #########################
class ImportTableProcessor:
    # File contains all the imported library functions in the dataset
    # Has one function per line. For example, "KERNEL32.DLL.SetEvent" where
    # the last part is the function name and the first part is the dll name.
    imported_func_list_file = "imported_func_list"
    def __init__(self, training_label_file):
        # mapping of instance_id -> class lable
        self.training_label = DataSetUtil.read_training_lable(training_label_file)
        # functions imported from external libs 
        self.imported_func_freq = {}
        # total number of external library function calls.
        self.total_ext_calls = 0.0
        # the imported function frequency feature vector csv header.
        self.f_vector_header = None
        
        self.reset_func_freq()
    
    def reset_func_freq(self):
        """
        Resets all the counts to zero.
        """
        self.total_ext_calls = 0.0
        if len(self.imported_func_freq) == 0:
            with open(ImportTableProcessor.imported_func_list_file) as import_file:
                for line in import_file:
                    line = line.strip()
                    if len(line) == 0:
                        continue
                    parts = line.split(".")
                    if len(parts) == 0:
                        continue
                    # i.e only the function name is used not the library name.
                    self.imported_func_freq[parts[len(parts)-1]] = 0.0
        else:
            for k in self.imported_func_freq:
                self.imported_func_freq[k] = 0.0
    
    def write_func_freq_vect_to_file(self, prev_filepath):
        in_filename = os.path.basename(prev_filepath)
        # get the hash representing the malware id from file name
        exe_hash = in_filename.split()[0].split('.')[0]
        if exe_hash in self.training_label and self.writer is not None:
            class_label = self.training_label[exe_hash]
            self.normalize_feature_vector()
            feat_vect = self.get_freq_feature_vector(class_label)
            feat_header = self.get_freq_feature_vector_header()
            feat_vect_obj = ExternalFuncCallInfo(feat_header, feat_vect)
            self.writer.write(feat_vect_obj)
            self.reset_func_freq()
            
    def file_changed_callback(self, prev_filepath, curr_filepath):
        """
        DataReader reading file changed event handler(callback function).
        """
        if  self.total_ext_calls == 0:
            return
        self.write_func_freq_vect_to_file()
        
        self.filecount = self.filecount + 1
        if self.filecount % 200 == 0:
            print "%d Files processed..." % (self.filecount)
        
    def normalize_feature_vector(self):
        """
        Normalizes the instruction frequency by dividing by the total 
        number of instructions observed in a binary.
        """
        if self.total_ext_calls == 0:
            return
        
        for key in self.imported_func_freq:
            self.imported_func_freq[key] = self.imported_func_freq[key] / self.total_ext_calls
    
    def get_freq_feature_vector(self, class_label):
        """
        Returns a the instruction freq count in the form of a row 
        where each colum, apart from the last one, represents a frquency of
        an instruction. the last colum is the class lable. 
        """
        f_vector = ''
        # for consistant column order sort dic by key
        first = True
        for key in sorted(self.imported_func_freq):
            if not first:
                f_vector = f_vector + ','
            
            f_vector =  f_vector + str(self.imported_func_freq[key])  
            first = False
            
        f_vector =  f_vector + ',' + class_label    
        return f_vector
            
    def get_freq_feature_vector_header(self):
        """
        Returns the header of instruction freq table.
        """
        if self.f_vector_header is not None:
            return self.f_vector_header
            
        # for consistant column order sort dic by key
        self.f_vector_header = ''
        first = True
        for key in sorted(self.imported_func_freq):
            if not first:
                self.f_vector_header = self.f_vector_header + ','
            
            self.f_vector_header =  self.f_vector_header + str(key)  
            first = False
        
            
        self.f_vector_header =  self.f_vector_header + ',class'      
        return str(self.f_vector_header)
     
    
    def extract_import_call_freq(self, inputpath, outputpath):
        """
        Extracts feature vetor, from each file on inputpath, and writes
        it to the requested output file. The feature is a frequecy of 
        calls made to functions imported from an external library/dll. 
        The frequency count is normalized by the number of total 
        function calls made in a given binary(asm file).
        """
        self.writer = DataWriter(outputpath)
        self.filecount = 0
        
        self.reader = DataReader(inputpath)
        
        curr_file = None
        for function in self.reader.data_line_iterator(data_parser = ExternalFuncCallInfo.parse_line, file_changed_callback_obj = self):
            if ".asm" not in self.reader.get_current_position()[0]:
                continue
            if function is None:
                continue
                
            # this is experimental
            if self.filecount >= 1000:
                break
                
            
            self.total_ext_calls = self.total_ext_calls + 1
            if function in self.imported_func_freq:
                self.imported_func_freq[function] = self.imported_func_freq[function] + 1
                
        # at the end self.reader.get_current_position()[0] will point to the last file opended.
        self.write_func_freq_vect_to_file(self.reader.get_current_position()[0])
        self.writer.close()
        
        
    @staticmethod
    def extract_all_imported_functions(input_path):
        """
        Extractes all the import table functions and store them in 
        import_map dictionary to avoid duplication
        """
        import_map = {}
        reader = DataReader(input_path)
        # The assumption is that the library name will bespecifed before 
        # the list of functions hence 
        current_library = ''
        
        for import_info in reader.data_line_iterator(data_parser = ImportTableInfo.parse_line):
            if import_info == None:
                continue
        
            if import_info.library_name is not '':
                current_library = import_info.library_name
                print current_library
                
            if import_info.function_name is None:
                continue
                
            key = current_library + '.' + import_info.function_name    
            if key not in import_map:
                import_map[key] = 0
            
            import_map[key] = import_map[key] + 1
            
        return import_map

###################### External Function Call Info #####################
class ExternalFuncCallInfo:
    def __init__(self, coln_header, coln_func_freq):
        self.coln_header = coln_header
        self.coln_func_freq = coln_func_freq
        
    @staticmethod
    def parse_line(line):
        """
        Parses a string and returns None if the required pattern is not found.
        Otherwise returns a string with the name of function called by a
        "call" assembly instruction.
        """
        import_call_regex = r'.text:[0-9A-F]+\s+(?:[0-9A-F][0-9A-F]\s)+\s+call\s+(\S+)'
        
        result = re.findall(import_call_regex, line)
        if len(result) == 0:
            return None
        
        func_name = re.findall(r'ds:(\S+)', result[0])
        if len(func_name) == 0:
            return None
        else:
            return func_name[0]
            
    def header(self):
        return self.coln_header
    
    def __str__(self):
        return self.coln_func_freq
        
class ImportTableInfo:
    def __init__(self, function_name = None, library_name = ''):
        self.function_name = function_name
        self.library_name = library_name
    
    @staticmethod
    def parse_line(line):
        import_library_regex = r'idata:[0-9A-Fa-f]+\s+;\s+Imports from\s+(\S+)'
        import_function_regex = r'idata:[0-9A-Fa-f]+\s+;\s+[A-Za-z]+\s+__[A-Za-z]+\s+([A-Za-z]+)\('
        
        regex_result = re.findall(import_function_regex, line)
        is_function = True
        
        if len(regex_result) < 1 or len(regex_result[0]) < 1:
            is_function = False
            regex_result = re.findall(import_library_regex, line)
            if len(regex_result) < 1 or len(regex_result[0]) < 1:
                return None
        
        if is_function:
            return ImportTableInfo(function_name = regex_result[0].strip())
        else:
            return ImportTableInfo(library_name = regex_result[0].strip())

########################## AsmFileProcessor ############################
class AsmFileProcessor:
    # A simple file containg list of all assembly instructions, one per line.
    inst_list_file = 'asm_inst_list'
    def __init__(self,  training_lable_file, instruction_freq_vector_mode = False, output_datawriter = None):
        """
        Parameters:
            - training_lable_file: file containing class labels of the training data.
            - instruction_freq_vector_mode: when True, used to calculate instruction
                frequency. When False, for each input file creates an output 
                file where each line contains opcode,instruction pair.
            - output_datawritter: DataWriter object when run in instruction_freq_vector_mode=True
        """
        self.writer = output_datawriter
        self.reader = None
        self.inst_vect_mode = instruction_freq_vector_mode
        self.inst_freq = {}
        self.total_inst = 0.0
        #initialize dictionary
        self.reset_inst_freq()
        # dictionary mapping the hash id of the binary to class label 
        self.lable_dic = DataSetUtil.read_training_lable(training_lable_file)
        self.f_vector_header = None
            
    def reset_inst_freq(self):
        """
        Resets all the counts to zero.
        """
        self.total_inst = 0.0
        if len(self.inst_freq) == 0 and self.inst_vect_mode is True:
            with open(AsmFileProcessor.inst_list_file) as inst_file:
                for line in inst_file:
                    line = line.strip()
                    if len(line) == 0:
                        continue
                    self.inst_freq[line] = 0.0
        else:
            for k in self.inst_freq:
                self.inst_freq[k] = 0.0
                
    def write_inst_vect_to_file(self, prev_filepath):
        if prev_filepath == None:
            return
            
        in_filename = os.path.basename(prev_filepath)
        # get the hash representing the malware id from file name
        exe_hash = in_filename.split()[0].split('.')[0]
        if exe_hash in self.lable_dic and self.writer is not None:
            class_label = self.lable_dic[exe_hash]
            self.normalize_feature_vector()
            feat_vect = self.get_freq_feature_vector(class_label)
            feat_header = self.get_freq_feature_vector_header()
            feat_vect_obj = FreqFeatureVector(feat_header, feat_vect)
            self.writer.write(feat_vect_obj)
            self.reset_inst_freq()
            
    def file_changed_callback(self, prev_filepath, curr_filepath):
        """
        DataReader reading file changed event handler(callback function).
        """
        if self.inst_vect_mode is True:
            if  self.total_inst == 0:
                return
            self.write_inst_vect_to_file(prev_filepath)
        else:
            if self.writer is None:
                return
            
            self.writer.close()
            self.writer = None

    def extract_opcode_inst(self, input_path, output_path):
        """
        Extracts the assembly instruction and its opcode and either writes
        them to csv file in the form of a two columns
        Parameters:
            input_path - file path to the file/folder containing data.
            output_path - path to the output folder.
        """
        self.reader = DataReader(input_path)
        for pair in self.reader.data_line_iterator(data_parser = OpcodeInstPair.parser_line, file_changed_callback_obj = self):
            if pair == None:
                continue

            # write the asm inst opcode pair to file
            if self.writer == None:
                in_filename = self.reader.get_current_position()[0]
                out_filename = output_path
                if in_filename[len(in_filename)-1] != '/' and in_filename[len(in_filename)-1] != '\\':
                    out_filename = output_path + os.path.basename(in_filename)
                
                print 'output file: ' + out_filename
                self.writer = DataWriter(out_filename)
                
            self.writer.write(pair)
                
        if self.writer is not None:
            self.writer.close()
    
    def extract_inst_freq(self, input_path):
        """
        Extracts instruction frequency.
        Parameters:
            input_path - file path to the file/folder containing data.
        """
        self.reader = DataReader(input_path)
        for pair in self.reader.data_line_iterator(data_parser = OpcodeInstPair.parser_line, file_changed_callback_obj = self):
            if pair == None:
                continue
                
            if self.inst_vect_mode is True: 
                if pair.inst in self.inst_freq:
                    self.inst_freq[pair.inst] = self.inst_freq[pair.inst] + 1
                    self.total_inst = self.total_inst + 1

        self.write_inst_vect_to_file(self.reader.get_current_position()[0])
        
    def normalize_feature_vector(self):
        """
        Normalizes the instruction frequency by dividing by the total 
        number of instructions observed in a binary.
        """
        if self.total_inst == 0:
            return
        
        for key in self.inst_freq:
            self.inst_freq[key] = self.inst_freq[key] / self.total_inst
    
    def get_freq_feature_vector(self, class_label):
        """
        Returns a the instruction freq count in the form of a row 
        where each colum, apart from the last one, represents a frquency of
        an instruction. the last colum is the class lable. 
        """
        f_vector = ''
        # for consistant column order sort dic by key
        first = True
        for key in sorted(self.inst_freq):
            if not first:
                f_vector = f_vector + ','
            
            f_vector =  f_vector + str(self.inst_freq[key])  
            first = False
            
        f_vector =  f_vector + ',' + class_label    
        return f_vector
            
    def get_freq_feature_vector_header(self):
        """
        Returns the header of instruction freq table.
        """
        if self.f_vector_header is not None:
            return self.f_vector_header
            
        # for consistant column order sort dic by key
        self.f_vector_header = ''
        first = True
        for key in sorted(self.inst_freq):
            if not first:
                self.f_vector_header = self.f_vector_header + ','
            
            self.f_vector_header =  self.f_vector_header + str(key)  
            first = False
        
            
        self.f_vector_header =  self.f_vector_header + ',class'      
        return str(self.f_vector_header)
                
            
########################## FreqFeatureVector ###########################
class FreqFeatureVector:
    def __init__(self, column_header, freq_feature_vect):
        self.column_header = column_header
        self.freq_feature_vect = freq_feature_vect
        
    def header(self):
        return self.column_header
    
    def __str__(self):
        return self.freq_feature_vect
        
################################ main ##################################
class RunningMode:
    inst_freq_archive = 1
    inst_freq_folder = 2
    opcode_inst_pair = 3
    extract_all_imports = 4
    imported_call_freq = 5

def main():

    mode_inst_vect = RunningMode.imported_call_freq
    
    input_path = argv[1]
    if mode_inst_vect is RunningMode.extract_all_imports:
        output_path = argv[2]
    elif mode_inst_vect is RunningMode.imported_call_freq:
        training_lable_file = argv[2]
        output_path = argv[3]
    else:
        training_lable_file = argv[2]
        output_path = argv[3]
    
    if mode_inst_vect is RunningMode.inst_freq_archive:
        data_writer = DataWriter(output_path)
        processor = AsmFileProcessor(training_lable_file, instruction_freq_vector_mode = True, output_datawriter = data_writer)
        # list archive content
        file_list = ArchiveUtil.list_contect(input_path)
        tmp_folder = 'tmp/'
        max_file = 1000
        
        count = 0
        for filename in file_list:
            if '.asm' not in filename:
                continue
            
            if count >= max_file:
                break
            count = count + 1  
            
            if count % 50 == 0:
                print '%d Files processed...' % count
            
            # extract one .asm file at a time
            ArchiveUtil.extract_one_file(input_path, filename, tmp_folder)
            
            # process each .asm file
            processor.extract_inst_freq(os.path.join(tmp_folder, os.path.basename(filename)))
            
            # delete extracted .asm file
            os.remove(os.path.join(tmp_folder, os.path.basename(filename)))
            
        # in the end
        data_writer.close()
        
    elif mode_inst_vect is RunningMode.inst_freq_folder:
        data_writer = DataWriter(output_path)
        processor = AsmFileProcessor(training_lable_file, instruction_freq_vector_mode = True, output_datawriter = data_writer)
        # if file then add to list if folder add all content to list
        filepath_list = []
        if os.path.isdir(input_path):
            filepath_list = [os.path.join(input_path, f) for f in listdir(input_path) if os.path.isfile(os.path.join(input_path, f))]
        else:
            filepath_list.add(input_path)
        # for each file
        count = 0 
        for filename in filepath_list: 
            if '.asm' not in filename:
                continue
            
            if count % 50 == 0:
                print '%05d Files processed...' % count
            # process each .asm file
            processor.extract_inst_freq(filename)
            
            count += 1
            
        # in the end
        data_writer.close()
    
    elif mode_inst_vect is RunningMode.extract_all_imports:
        imports = ImportTableProcessor.extract_all_imported_functions(input_path)
        writer = DataWriter(output_path)
        #for key, value in imports.iteritems():
        l = sorted(imports.keys())
        for key in l:
            writer.write_str_line(key)
        
    elif mode_inst_vect is RunningMode.imported_call_freq:
        processor = ImportTableProcessor(training_lable_file)
        processor.extract_import_call_freq(input_path, output_path)
        
    else:    
        processor = AsmFileProcessor(training_lable_file)
        processor.extract_opcode_inst(input_path, output_path)

    return 0

if __name__ == '__main__':
	main()

