#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  dataset_util.py
#  
#  Copyright 2015 meha <meha@meha-VPCSB16FG>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import re

class DataSetUtil:
    """
    Utility class providing helper function with regards to training and 
    test dataset.
    """
    @staticmethod
    def read_training_lable(training_lable_file):
        """
        Read the class lable information from the training data. 
        The file is in a csv format with two columns <instance_id, class_label> 
        and a file header.
        """
        dic = {}
        with open(training_lable_file, 'r') as f:
            first = True
            for line in f:
                if first:
                    first = False
                    continue
                cols = line.split(',')
                if len(cols) != 2:
                    continue
                
                dic[cols[0].strip('"')] = cols[1].strip()
        
        return dic

    
    @staticmethod
    def read_instruction_opcode_list(control_inst_file):
        """
        Space separated column file. first column is instruction mnemonic next columns are opcode.
        """
        regex = r'([A-Za-z]+)\s+(.*)'
        opcode_to_inst = {}
        with open(control_inst_file) as fin:
            for line in fin:
                result = re.findall(regex, line)
                if len(result) > 0 and len(result[0]) != 2:
                    continue
                
                opcode_to_inst[result[0][1].strip()] = result[0][0]
                
        return opcode_to_inst

def main():
	
	return 0

if __name__ == '__main__':
	main()

