#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  n_gram_freq.py
#
#  Copyright 2015 mehadi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
#  Dependancies:
#       - mmh3 murmurhash3 python wrapper 2.3
#

import argparse
import ctypes
from collections import deque
import math
import mmh3
import os.path
from simhash import Simhash

from dataset_util import DataSetUtil
from file_util import DataReader
from file_util import DataWriter
from instruction_util import OpcodeInstPair


class NGramFeatures:
    """
    This class provides methods for extracting n-gram features from  a
    disassemble binary/program.
    """
    # Supported hashing techniques
    simhash = 'simhash'
    murmurhash = 'murmurhash'
    nohash = 'nohash'

    def __init__(self, training_label_file, control_inst_file = None, print_row_id=False, skip_zero_counts=True):
        """
        Parameters:
            control_inst_file - if set, path to list of control instructs and thier opecode space separated column format. first column is instruction mnemonic and next columns are for opcode.
            print_row_id - if True, print row id when printing feature vectors to file.
            skip_zero_counts - if True, skip data instance that don't even have a single instruction.
        """
        self.training_label = None
        if training_label_file is not None:
            self.training_label = DataSetUtil.read_training_lable(training_label_file)
        
        # Dictionary mapping opcode to mnemonics
        self.control_inst_dic = None
        if control_inst_file is not None:
            self.control_inst_dic = DataSetUtil.read_instruction_opcode_list(control_inst_file)
        
        # If True, The first column of each row is the rowId.
        self.print_row_id = print_row_id
        self.skip_zero_counts = skip_zero_counts

    def reset_inst_freq(self):
        """
        Resets all the counts to zero.
        """
        # Total number of instruction in a disassembled binary file.
        self.total_ngrams = 0.0
        # Previous ngram_size instruction Mnemonics observered in a 
        # disassembled binary file.
        self.inst_queue = deque(maxlen=self.ngram_size)

        if self.buckets is None:
            self.buckets = []
            if self.hash_fn != NGramFeatures.nohash:
                for i in range(0, self.num_hash_buckets):
                    self.buckets.append(0)
            else:
                self.all_ngrams_dic = {}
                inst_list = []
                with open(self.inst_list_file) as inst_f:
                    for line in inst_f:
                        line = line.strip()
                        if len(line) == 0:
                            continue
                        inst_list.append(line)

                index = 0
                for i in range(0, len(inst_list)):
                    for j in range(i, len(inst_list)):
                        if i == j:
                            continue
                        bigram = self.ngram_to_str([inst_list[i], inst_list[j]])
                        
                        if bigram not in self.all_ngrams_dic:
                            self.all_ngrams_dic[bigram] = index
                            index = index + 1
                            self.buckets.append(0)
                
                print self.all_ngrams_dic

        else:
            for i in range(0, self.num_hash_buckets):
                self.buckets[i] = 0
    
    
    def resolve_class_label(self, filename):
        """
        Returns the class label of the binary file from filename.
        Returns None if class label can not be resolved from filename.  
        """
        if filename in self.training_label and self.writer is not None:
            return self.training_label[filename]
            
        return None
           
    
    def get_freq_feature_vector(self, class_label, row_id):
        """
        Returns a the instruction hash bucket freq count in the form of a row 
        where each colum, apart from the last one, represents a frquency of
        a hash bucket. the last colum is the class lable. 
        Parameters:
            class_label - the class label of data instance. If this is 
                          set to None, then class label will not be added.
        """
        if self.print_row_id:
            f_vector = row_id
            first = False
        else:
            f_vector = ''
            first = True
        
        for elem in self.buckets:
            if not first:
                f_vector = f_vector + ','
            
            if self.total_ngrams != 0:
                f_vector = f_vector + str(elem / self.total_ngrams)
            else:
                f_vector = f_vector + '0.0'
            first = False
        
        if class_label is not None:
            f_vector = f_vector + ',' + class_label
        return f_vector
        
        
    def get_freq_feature_vector_header(self, has_class=True):
        """
        Returns the header of instruction hash bucket freq table.
        Parameters:
            has_class - by default it is True. when set to False table 
                        header will not include 'class' column.
        """
        if self.f_vector_header is not None:
            return self.f_vector_header
        
        if self.print_row_id:
            self.f_vector_header = 'row_id'
            first = False
        else:    
            self.f_vector_header = ''
            first = True
            
        for index in range(0, len(self.buckets)):
            if not first:
                self.f_vector_header = self.f_vector_header + ','
            
            self.f_vector_header = self.f_vector_header + str(index)
            first = False
        
        if has_class:
            self.f_vector_header = self.f_vector_header + ',class'
        return self.f_vector_header
        
        
    def write_ngram_freq_vect_to_file(self, prev_filename):
        """
        Writes the ngram feature for the previously processed file to the
        output file.
        """
        if prev_filename is None:
            return  # First file just opened.
        if  self.total_ngrams == 0 and self.skip_zero_counts:
            return
        
        in_filename = os.path.basename(prev_filename)
        exe_hash = in_filename.split('.')[0]
        if self.training_label is not None:    
            class_label = self.resolve_class_label(exe_hash)
            if class_label == None:
                return
        else:
            class_label = None # in case of test data without class label.

        feat_vect = self.get_freq_feature_vector(class_label, exe_hash)
        feat_header = self.get_freq_feature_vector_header(has_class=(class_label is not None))
        feat_vect_obj = NGramFreqInfo(feat_header, feat_vect)
        self.writer.write(feat_vect_obj)
        #print "%s writing success!" % exe_hash
        self.reset_inst_freq()
    
        
    def file_changed_callback(self, prev_filename, curr_filename):
        """
        Handles file changed event raised by DataReader when ever it 
        opens a new file. 
        """
        #print 'File changed prev="%s" new="%s"' % (prev_filename, curr_filename)
        self.write_ngram_freq_vect_to_file(prev_filename)
        
        self.filecount = self.filecount + 1
        if self.filecount % 200 == 0:
            print "%d Files processed..." % (self.filecount)
    
       
    def hash_simhash(self, collection):
        """
        Hashs a collection using Simhash, which a locality sensative hashing
        algorithm. Simhash hashes similar elements closes to each other.
        """
        return Simhash(collection, f = self.hash_size).value
        
        
    def hash_str(self, str_to_hash):
        """
        Hashs a given string using MurmurHash, which uniformly distributes 
        elements accros buckets.
        """
        mmh3_hash = mmh3.hash(str_to_hash)
        unsigned_mmh3_hash = ctypes.c_uint32(mmh3_hash).value
        return unsigned_mmh3_hash % self.num_hash_buckets
     
        
    def ngram_to_str(self, l):
        n_gram = ''
        first = True
        for elem in l:
            if not first:
                n_gram = n_gram + ','
            
            n_gram = n_gram + elem
            first = False
        
        return n_gram
     
        
    def update_ngram_freq(self):
        """
        Hashs the current n-gram, and use hash as index to update the 
        approprate bucket.
        """
        # If control instruction list is set then, we need to consider 
        # ngrams starting with control instructions only. 
        if self.control_inst_dic != None and self.inst_queue[0] not in self.control_inst_dic:
            return
        
        #print self.inst_queue
        
        self.total_ngrams = self.total_ngrams + 1
        
        if self.hash_fn == NGramFeatures.murmurhash:
            n_gram = self.ngram_to_str(self.inst_queue)
            #print self.inst_queue
            index = self.hash_str(n_gram)
        elif self.hash_fn == NGramFeatures.simhash:
            index = self.hash_simhash(self.inst_queue)
        elif self.hash_fn == NGramFeatures.nohash:
            n_gram = self.ngram_to_str(self.inst_queue)  
            if n_gram not in self.all_ngrams_dic:
                return
            index = self.all_ngrams_dic[n_gram]
        else:
            print 'Hash function "%s" Not supported!' % self.hash_fn
            exit(1) 
            
        self.buckets[index] = self.buckets[index] + 1
    
            
    def extract_n_gram_features(self, n, hash_size, input_path, output_path, hash_fn = murmurhash, opcode_mode=False, max_files=-1, inst_list_file=None):
        """
        Extracts n-gram fetures, which is the frequecty of n-grams, from 
        instruction Mnemonics from a disassembled binary. Because the 
        size n-gram features grows exponentialy in the size of the 
        instruction set and n, we use Hashing Trick to reduce the 
        feature space size.
        
        Parameters:
            n           - the n-gram length
            hash-size   - the number of bits used by the hash function
            input_path  - path to the input .asm disassembly files
            output_path - path to output to write extracted features 
            hash_fn     - the hash function to be used.
            max_files   - maximum number of malware disasssembly files to process
            inst_list_file - only in case of nohash bigrams
            opcode_mode - if true, the intruction ope code is used for n-grams instead of mnemonics
        """
        self.reader = DataReader(input_path)
        self.writer = DataWriter(output_path)
        # hash function to use
        self.hash_fn = hash_fn
        # the number of hash buckets
        self.num_hash_buckets = int(math.pow(2, hash_size))
        # number of bits 
        self.hash_size = hash_size
        # hash buckets for string instruction frequecy counts
        self.buckets = None
        self.ngram_size = n
        # CSV header row for table created using these features
        self.f_vector_header = None
        # Numebr of files proccessed
        self.filecount = 0
        # file path to asm instruction list. this only used in case when no hashing trick is requested.
        self.inst_list_file = inst_list_file
        
        # initilize enviroment
        self.reset_inst_freq()
        
        for pair in self.reader.data_line_iterator(data_parser = OpcodeInstPair.parser_line, file_changed_callback_obj = self, file_extension = '.asm'):
            if pair == None:
                continue
            
            # this is experimental
            if max_files != -1 and self.filecount >= max_files:
                break
            
            # add new element
            if opcode_mode: # in opcode mode add the instruction opcode
                inst_opcode = pair.get_opcode()
                if inst_opcode is not None:
                    self.inst_queue.append(inst_opcode)
            else:   # in non-opcode mode add instruction mnemonics
                self.inst_queue.append(pair.inst)
            
            # if n-gram is not full, does not have n elements, continue. 
            if len(self.inst_queue) < n:
                continue
            
            # update frequecy
            self.update_ngram_freq()
            
        # write the ngram freq for the last file
        self.write_ngram_freq_vect_to_file(self.reader.get_current_position()[0])
        self.writer.close()
        

class NGramFreqInfo:
    def __init__(self, feat_header, feat_vector):
        self.feat_header = feat_header
        self.feat_vector = feat_vector
        
    def header(self):
        return self.feat_header
        
    def __str__(self):
        return self.feat_vector


def main():
    # command line args
    parser = argparse.ArgumentParser(description='Preprocess disassembled  malware file to generate n-gram frequency features.')
    parser.add_argument('-in', required=True, dest='input_path', help='path to disassembled malware file or directory.')
    parser.add_argument('-label', type=str, default=None, dest='training_label_file', help='path to file containing the training label file')
    parser.add_argument('-out', required=True, dest='output_path', help='path to output file or directory.')
    parser.add_argument('-n', type=int, default=2, dest='n_gram_len', help='N-gram length')
    parser.add_argument('-hash', default=NGramFeatures.murmurhash, dest='hash_fn', help='hash function name. Currently supported "murmurhash", "simhash" or "nohash". "nohash" supported for bigrams only.')
    parser.add_argument('-hsize', type=int, default=8, dest='hash_bits', help='number of bits used to represent hash result')
    parser.add_argument('-max', type=int, default=-1, dest='max_files', help='max number of assembly files to process.')
    parser.add_argument('-inst', default='asm_inst_list', dest='asm_inst_file', help='file containing a set of assembly instructions, one per line.')
    parser.add_argument('-opcode', type=bool, default=False, dest='opcode_mode', help='extract opcode n-grams instead of instruction mnemonic ngrams.')
    parser.add_argument('-no-skip-zero', type=bool, default=False, dest='no_skip_zero', help='print zero feature values for files with intruction total count of zero.')
    parser.add_argument('-control', default=None, dest='control_inst_file', help='list of control instructs and thier opecode space separated column format. first column is instruction mnemonic and next columns are for opcode.')
    
    # parse command line args 
    args = parser.parse_args()
    
    #input_path = argv[1]
    #training_label_file = argv[2]
    #output_path = argv[3]
    
    #n_gram_len = 4
    #hash_bits = 8
    
    print "Extracting %d-grams using %d-bit %s hashing fn. opcode_mode=%s" % \
            (args.n_gram_len, args.hash_bits, args.hash_fn, str(args.opcode_mode))
    
    ngram_processor = NGramFeatures(args.training_label_file, \
                                    args.control_inst_file, \
                                    args.training_label_file is None, \
                                    skip_zero_counts=not args.no_skip_zero)
    ngram_processor.extract_n_gram_features(args.n_gram_len, \
                                            args.hash_bits, \
                                            args.input_path, \
                                            args.output_path, \
                                            hash_fn = args.hash_fn, \
                                            max_files=args.max_files, \
                                            opcode_mode=args.opcode_mode, \
                                            inst_list_file=args.asm_inst_file)
    
    return 0


if __name__ == '__main__':
	main()

