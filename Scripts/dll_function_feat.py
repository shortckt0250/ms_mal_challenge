#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  dll_function_feat.py
#  
#  Copyright 2015 meha <meha@meha-VPCSB16FG>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import argparse
import ctypes
import math
import mmh3
import os.path

from dataset_util import DataSetUtil
from dll_util import ImportTableInfo
from file_util import DataReader
from file_util import DataWriter


class DLLFunctionBoolFeature:
    """
    This class provides methods for extracting imported functions boolean
    features from  a disassembled binary/program file.
    """
    murmurhash = 'murmurhash'
    def __init__(self, training_label_file, print_row_id=False):
        # mapping of instance_id -> class lable
        self.training_label = None
        if training_label_file is not None:
            self.training_label = DataSetUtil.read_training_lable(training_label_file)
        # If True, The first column of each row is the rowId.
        self.print_row_id = print_row_id


    def reset_inst_freq(self):
        """
        Resets all the counts to zero.
        """
        if self.buckets is None:
            self.buckets = []
            for i in range(0, self.num_hash_buckets):
                self.buckets.append(0)
        else:
            for i in range(0, self.num_hash_buckets):
                self.buckets[i] = 0

    
    def resolve_class_label(self, filename):
        """
        Returns the class label of the binary file from filename.
        Returns None if class label can not be resolved from filename.  
        """
        if filename in self.training_label:
            return self.training_label[filename]
            
        return None
        

    def get_dll_func_feature_vector(self, class_label, row_id):
        """
        Returns a the hash bucket in the form of a row  where each colum, 
        apart from the last one, represents a content of the  hash bucket. 
        the last colum is the class lable if class_label parameter is 
        not None. 
        Parameters:
            class_label - the class label of data instance. If this is 
                          set to None, then class label will not be added.
        """
        if self.print_row_id:
            f_vector = row_id
            first = False
        else:
            f_vector = ''
            first = True
        
        for elem in self.buckets:
            if not first:
                f_vector = f_vector + ','
            
            f_vector = f_vector + str(elem)
            first = False
        
        if class_label is not None:
            f_vector = f_vector + ',' + class_label
        return f_vector
        

    def get_dll_func_feature_vector_header(self, has_class=True):
        """
        Returns the header of hash bucket dll function bool table.
        Parameters:
            has_class - by default it is True. when set to False table 
                        header will not include 'class' column.
        """
        if self.f_vector_header is not None:
            return self.f_vector_header
        
        if self.print_row_id:
            self.f_vector_header = 'row_id'
            first = False
        else:    
            self.f_vector_header = ''
            first = True
            
        for index in range(0, len(self.buckets)):
            if not first:
                self.f_vector_header = self.f_vector_header + ','
            
            self.f_vector_header = self.f_vector_header + str(index)
            first = False
        
        if has_class:
            self.f_vector_header = self.f_vector_header + ',class'
        return self.f_vector_header
        

    def write_dll_func_feat_to_file(self, prev_filename):
        """
        Writes the boolean dll function feature of the previously 
        processed file to the output file.
        """
        
        if prev_filename is None:
            return  # First file just opened.
        
        in_filename = os.path.basename(prev_filename)
        exe_hash = in_filename.split('.')[0]
        if self.training_label is not None:    
            class_label = self.resolve_class_label(exe_hash)
            if class_label == None:
                return
        else:
            class_label = None # in case of test data without class label.

        feat_vect = self.get_dll_func_feature_vector(class_label, exe_hash)
        feat_header = self.get_dll_func_feature_vector_header(has_class=(class_label is not None))
        feat_vect_obj = DLLFunctionBoolFeat(feat_header, feat_vect)
        self.writer.write(feat_vect_obj)
        self.reset_inst_freq()
    

    def file_changed_callback(self, prev_filename, curr_filename):
        """
        Handles file changed event raised by DataReader when ever it 
        opens a new file. 
        """
        self.write_dll_func_feat_to_file(prev_filename)
        
        self.filecount = self.filecount + 1
        if self.filecount % 200 == 0:
            print "%d Files processed..." % (self.filecount)
        

    def hash_str(self, str_to_hash):
        """
        Hashs a given string using MurmurHash, which uniformly distributes 
        elements accros buckets.
        """
        mmh3_hash = mmh3.hash(str_to_hash)
        unsigned_mmh3_hash = ctypes.c_uint32(mmh3_hash).value
        return unsigned_mmh3_hash % self.num_hash_buckets


    def update_bucket(self, dll_func):
        if self.hash_fn == DLLFunctionBoolFeature.murmurhash:
            index = self.hash_str(dll_func)
        else:
            print 'Hash function "%s" Not supported!' % self.hash_fn
            exit(1) 

        self.buckets[index] = 1


    def extract_dll_function_bool_features(self, hash_size, input_path, output_path, hash_fn = murmurhash, max_files=-1):
        """
        Extracts the exisstance or abcense of a .We use Hashing Trick to reduce the 
        feature space size.
        
        Parameters:
            hash-size   - the number of bits used by the hash function
            input_path  - path to the input .asm disassembly files
            output_path - path to output to write extracted features 
            hash_fn     - the hash function to be used.
            max_files   - maximum number of malware disasssembly files to process
        """
        self.reader = DataReader(input_path)
        self.writer = DataWriter(output_path)
        # hash function to use
        self.hash_fn = hash_fn
        # the number of hash buckets
        self.num_hash_buckets = int(math.pow(2, hash_size))
        # number of bits 
        self.hash_size = hash_size
        # hash buckets for boolean dll function 
        self.buckets = None
        # CSV header row for table created using these features
        self.f_vector_header = None
        # Numebr of files proccessed
        self.filecount = 0
        
        # initilize enviroment
        self.reset_inst_freq()
        
        current_dll = ''
        for import_info in self.reader.data_line_iterator(data_parser = ImportTableInfo.parse_line, file_changed_callback_obj = self, file_extension = '.asm'):
            if import_info == None:
                continue
            
            if max_files != -1 and self.filecount >= max_files:
                break
            
            if import_info.function_name is None:
                if import_info.library_name != '':
                    current_dll = import_info.library_name
                    
                continue
            
            self.update_bucket('%s.%s' % (import_info.library_name, import_info.function_name))
            
        # write the ngram freq for the last file
        self.write_dll_func_feat_to_file(self.reader.get_current_position()[0])
        self.writer.close()
        
        
class DLLFunctionBoolFeat:
    def __init__(self, feat_header, feat_vector):
        self.feat_header = feat_header
        self.feat_vector = feat_vector
        
    def header(self):
        return self.feat_header
        
    def __str__(self):
        return self.feat_vector


def main():
    # command line args
    parser = argparse.ArgumentParser(description='Preprocess disassembled  malware file to generate n-gram frequency features.')
    parser.add_argument('-in', required=True, dest='input_path', help='path to disassembled malware file or directory.')
    parser.add_argument('-label', type=str, default=None, dest='training_label_file', help='path to file containing the training label file')
    parser.add_argument('-out', required=True, dest='output_path', help='path to output file or directory.')
    parser.add_argument('-hash', default=DLLFunctionBoolFeature.murmurhash, dest='hash_fn', help='hash function name. Currently supported "murmurhash" supported for bigrams only.')
    parser.add_argument('-hsize', type=int, default=8, dest='hash_bits', help='number of bits used to represent hash result')
    parser.add_argument('-max', type=int, default=-1, dest='max_files', help='max number of assembly files to process.')
    parser.add_argument('-rowid', type=bool, default=False, dest='include_row_id', help='Boolean value to include row_id column.')
    
    # parse command line args 
    args = parser.parse_args()

    processor = DLLFunctionBoolFeature(args.training_label_file, args.include_row_id)
    processor.extract_dll_function_bool_features(args.hash_bits, \
                                                 args.input_path, \
                                                 args.output_path, \
                                                 hash_fn = args.hash_fn, \
                                                 max_files= args.max_files)
    return 0

if __name__ == '__main__':
	main()

