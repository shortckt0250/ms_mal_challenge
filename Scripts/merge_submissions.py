#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  marge_submissions.py
#  
#  Copyright 2015 meha <meha@meha-VPCSB16FG>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import argparse
import re


def main():
    parser = argparse.ArgumentParser(description='merges testset predictions from the two different models. One being the opcode-ngram and the other using dll-boolean-feature')
    parser.add_argument('-t', required=True, dest='test_csv_path', help='path to csv file containint test instances for 2_gram_12_bit')
    parser.add_argument('-dll', required=True, dest='dll_submission', help='path to submission file for predictions made based on dll boolean feature model')
    parser.add_argument('-ngram', required=True, dest='ngram_submission', help='path to submission file for predictions made based on ngram feature model')
    parser.add_argument('-out', required=True, dest='merged_submission_file', help='path to the merged submision file')
    
    
    # parse command line args 
    args = parser.parse_args()
    
    pattern = r'(,0.0){4095}'
    
    header = ''
    dll_submission = []
    with open(args.dll_submission) as fin:
        first = True
        for line in fin:
            if first:
                first = False
                header = line
                continue
            dll_submission.append(line)

    
    ngram_submission = []
    with open(args.ngram_submission) as fin:
        first = True
        for line in fin:
            if first:
                first = False
                header = line
                continue
            ngram_submission.append(line)
    
    fout = open(args.merged_submission_file,'w')
    fout.write(header)
    with open(args.test_csv_path) as fin:
        first = True
        i = 0
        for line in fin:
            if first:
                first = False
                continue
                
            match = re.findall(pattern, line)
            if len(match) != 0:
                # get prediction from dll-model
                fout.write(dll_submission[i])
                print dll_submission[i].split(',')[0]
            else:
                # get prediction from ngram-model
                fout.write(ngram_submission[i])
            
            i = i + 1
    
    return 0

if __name__ == '__main__':
	main()

