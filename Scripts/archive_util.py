#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  archive_util.py
#
#    Utility module for dealing with archives. The module acts as a
#    wrapper class around the "7z" command in linux, which part of 
#    the p7zip 
#       * List archive file content
#       * extract a specific file from archive.
#
#
#
#
#  Copyright 2015 mehadi
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from sys import argv
from subprocess import PIPE, Popen, call
import os.path
import re

class ArchiveUtil:
    """
    Utility module for dealing with archives. The module acts as a
    wrapper class around the "7z" command in linux, which part of 
    the p7zip 
       * List archive file content
       * extract a specific file from archive.
       
    Dependencies:
        * Linux system with p7zip installed.
    """
    @staticmethod
    def list_contect(archive_path):
        """
        Lists the contect of a given archive file. 
        Parameters:
            - archive_path: path to archive  file.
        Returns:
            - list of all the files in the given archive.
        """
        if not os.path.exists(archive_path):
            print 'Error: File not found %s' %  archive_path
            return None
        
        cmd_arg = ['7z',  'l', archive_path]
        proc = Popen(cmd_arg, stdout=PIPE, stderr=PIPE)
        output, err = proc.communicate()
        
        start_end_regex = r'^----'
        in_table = False
        
        file_list = []
        
        for line in output.split('\n'):
            result = re.findall(start_end_regex, line)
            if len(result) > 0 and in_table:
                # end of table processing
                break
            elif len(result) > 0 and not in_table:
                in_table = True
            elif in_table:
                # process row
                cols = line.strip(' ').split(' ')
                if len(cols) == 0:
                    continue
                file_list.append(cols[len(cols) - 1])
        
        return file_list
        
    @staticmethod    
    def extract_one_file(archive_path, extract_filename, output_folder):
        """
        Extract a single file from an archive. 
        Parameters:
            - archive_path: path to archive  file.
            - extract_filename: name of the file to extracted
            - output_folder: output destination for the extracted file
        Returns:
            - True on success. and False on failure.
        """
        cmd_arg = ['7z',  'e', archive_path, '-o' + output_folder, extract_filename, '-r']
        ret = call(cmd_arg)
        
        return ret == 0


def main():
    file_list = ArchiveUtil.list_contect(argv[1])
    
    if len(argv) == 3:
        ArchiveUtil.extract_one_file(argv[1], file_list[0], argv[2])
    
    return 0

if __name__ == '__main__':
    main()
