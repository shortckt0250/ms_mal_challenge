#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  merge_dll_ngram_features.py
#  
#  Copyright 2015 meha <meha@meha-VPCSB16FG>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import argparse
import re


def write_header(fout):
    fout.write('%1. Title: Microsoft Malware Classification Challenge DataSet ngram + dll\n')
    fout.write('%\n')
    fout.write('@RELATION ngram_and_dll_dattaset\n\n')
    
    
def read_attributes(fin, fout):    
    """
    Reads the None DATA section of an ARFF file from input file fin.
    """
    class_line = None
    while True:
        try:
            line = fin.next()
        except:
            break
        
        if len(line.strip()) == 0:
            continue
        elif re.match(r'@[D|d][A|a][T|t][A|a]', line):
            #print 'breaking at %s' % line
            break
        elif re.match(r'%', line) != None:
            continue
        elif re.match(r'@[A|a][T|t][T|t][R|r][I|i][B|b][U|u][T|t][E|e]', line):
            if re.search(r'[C|c]lass', line):
                class_line = line
                continue
            
            fout.write(line)
    
    return class_line


def remove_last_column(line):
    """
    Removes the last column from a comma separated row.
    Returns tuple (modified_line, removed column)
    """
    i = len(line)
    while i >= 0:
        i = i - 1
        if line[i] == ',':
            break
        
    return (line[:i], line[i+1:])

def merge_data_lines(line_dll, line_ngram, has_class_line_dll, has_class_line_ngram):
    """
    Merges the two data lines line_dll and line_ngram.
    Assumes both lines dont have row_id
    """
    line_dll = line_dll.strip()
    line_ngram = line_ngram.strip()
    class_label = None
    
    if has_class_line_dll:
        result = remove_last_column(line_dll)
        line_dll = result[0]
        class_label = result[1]
    if has_class_line_ngram:
        result = remove_last_column(line_ngram)
        line_ngram = result[0]
        class_label = result[1]
    
    if class_label != None:
        return '%s,%s,%s' % (line_dll, line_ngram, class_label)
    else:
        return '%s,%s' % (line_dll, line_ngram)
        

def merge_dll_ngram_files(ngram_file, dll_bool_file, output_path):
    """
    Both should be ARFF files.
    No row_id column
    If class label is included, then should have header name 'class'.
    and should be the last column.
    and we assume the rows are in same order.
    """
    fdll = open(dll_bool_file)
    fngram = open(ngram_file)
    fout = open(output_path, 'w')
    
    write_header(fout)
    
    class_line_dll = read_attributes(fdll, fout)
    class_line_ngram = read_attributes(fngram, fout)
    class_line = None
    
    if class_line_dll != None:
        class_line = class_line_dll
    elif class_line_ngram != None:
        class_line = class_line_ngram
    
    if class_line != None:
        fout.write(class_line)
        
    fout.write('\n@DATA\n')
    
    while True:
        try:
            line_dll = fdll.next()
            line_ngram = fngram.next()
        except:
            break
        
        line_merged = merge_data_lines(line_dll, line_ngram, \
                                       class_line_dll != None, \
                                       class_line_ngram != None)
        
        fout.write(line_merged)
        fout.write('\n')
    
    fout.close()
    

def main():
    parser = argparse.ArgumentParser(description='Merges ngram based and boolean dll based features in ARFF file.')
    parser.add_argument('-ngram', required=True, dest='ngram_file', help='path to ARFF ngram feature file')
    parser.add_argument('-dll', required=True, dest='dll_file', help='path to ARFF dll boolean file ')
    parser.add_argument('-out', required=True, dest='merged_arff_file', help='path to the merged ARFF file')
    
    # parse command line args 
    args = parser.parse_args()
    
    merge_dll_ngram_files(args.ngram_file, args.dll_file, args.merged_arff_file)
    
    return 0

if __name__ == '__main__':
	main()

