#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  separate)dataset.py
#  
#  

from sys import argv
import random

def main():
    i = 0
    train = open('result/train.csv','w')
    test = open('result/test.csv','w')
    test_fraction = 0.3
    with open(argv[1]) as f:
        for line in f:
            if i == 0:
                train.write(line)
                test.write(line)
                i = i + 1
                continue
            
            rand = random.random()    
            if rand > test_fraction:
                train.write(line)
            else:
                test.write(line)
            
            i = i + 1    
                
    train.close()
    test.close()
    return 0

if __name__ == '__main__':
	main()

