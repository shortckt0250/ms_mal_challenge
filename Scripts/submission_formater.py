#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2015 mehadi
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import argparse
import re

def probabilistic_prediction(fout, instance_id, result_tuples):
    for i in range(0, len(instance_id)):
        line = instance_id[i]
        for j in range(1, 10):
            if j == result_tuples[i][1]:
                line = line + ',' + str(result_tuples[i][2])
            else:
                line = line + ',' + str((1-result_tuples[i][2]) / 8.0)
            
        fout.write(line + '\n')


def binary_prediction(fout, instance_id, result_tuples):
    for i in range(0, len(instance_id)):
        line = instance_id[i]
        for j in range(1, 10):
            if j == result_tuples[i][1]:
                line = line + ',' + '1.0'
            else:
                line = line + ',' + '0.0'
            
        fout.write(line + '\n')


def main():
    parser = argparse.ArgumentParser(description='converts testset predictions to submission format')
    parser.add_argument('-t', required=True, dest='test_csv_path', help='path to csv file containint test instances')
    parser.add_argument('-p', required=True, dest='prediction_path', help='path to file containing weka prediction')
    parser.add_argument('-out', required=True, dest='submission_file', help='path to submision file')
    parser.add_argument('-prob', type=bool, dest='prob', help='output probabilistic prediction.')
    
    # parse command line args 
    args = parser.parse_args()
    
    instance_id = []
    with open(args.test_csv_path) as csv:
        first = True
        for line in csv:
            if first:
                first = False
                continue
            
            cols = line.split(',')
            instance_id.append(cols[0])
    
    reg_ex = r'([0-9]+)\s+[0-9]+:([?0-9]+)\s+[0-9]+:([0-9]+)\s+([0-9]*[.0-9]+)'
    
    result_tuples = []
    with open(args.prediction_path) as fin:
        for line in fin:
            match = re.findall(reg_ex, line)
            
            if len(match) == 0:
                continue
            if len(match[0]) != 4:
                continue
            
            result_tuples.append((int(match[0][0]), int(match[0][2]), float(match[0][3])))
            
    if len(instance_id) != len(result_tuples):
        print 'Error: testset length (%d) not equal to prediction lenth (%d)' % (len(instance_id), len(result_tuples))
        return 1
        
        
    fout = open(args.submission_file, 'w')
    header = 'Id'
    for i in range(1, 10):
        header = header + ',Prediction' + str(i)
        
    fout.write(header + '\n')
    
    if args.prob:
        probabilistic_prediction(fout, instance_id, result_tuples)
    else:
        binary_prediction(fout, instance_id, result_tuples)
    
    fout.close()
    
    return 0

if __name__ == '__main__':
	main()

