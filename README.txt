= Files =
Experiments/
    - contains experiment results.
    
Scripts/
    -archive_util.py
        - Contains utility functions for reading files in archive. 
    -arff_generator.py
        - Convert a .csv file into .arff file for Weka 
    -dataset_util.py
        - Utility module providing helper function with for working with to training and 
    test dataset.
    -dll_features.py
        - Preprocess disassembled  malware file to generate boolean dll features.
    -dll_function_feat.py
        - Extracts external function call frequency feature. 
    -dll_util.py
        - Utility class for processing import table information in the malware dataset.
    -file_util.py
        - Utility classes for batch reading and writing to files.
    -instruction_util.py
        - Utility class for extracting assembly instruction and opcode from disassembly file 
    -merge_dll_ngram_features.py
        - merge dll and n-gram features into one feature vector
    -n_gram_freq.py
        - Extracts n-gram frequency feature.
    -opcode_inst_extractor.py
        - Extracts assembly instruction frequency feature
    -submission_formater.py
        - Convert prediction output into competition submission format. 
        
